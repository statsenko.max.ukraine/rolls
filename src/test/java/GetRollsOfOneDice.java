import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utils.APIPathes;
import utils.CounterOneDice;

public class GetRollsOfOneDice {


    @Parameters("oneDice")
    @Test
    public void getRolls(String oneDice) {

        int intParameter = Integer.parseInt(oneDice);
        int numberOfTimes = 0;

        while (numberOfTimes <= intParameter) {
            numberOfTimes++;

            RequestApi requestApi = new RequestApi();
            final String receivedResponse = requestApi.getRequest(200, APIPathes.oneRoll);
            CounterOneDice.numberCounter(receivedResponse);
        }
        System.out.println("1= " + CounterOneDice.countOne + " 2= " + CounterOneDice.countTwo + " 3= " + CounterOneDice.countTree + " 4= " + CounterOneDice.countFore + " 5= " + CounterOneDice.countFive + " 6= " + CounterOneDice.countSix);
        CounterOneDice.percentDeviation(intParameter);
    }
}
