import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utils.APIPathes;
import utils.CounterTwoDices;

public class GetRollsOfTwoDices {

    @Parameters("twoDices")
    @Test
    public void getRollsTwo(String twoDices) {

        int intParameter = Integer.parseInt(twoDices);
        int numberOfTimes = 0;

        while (numberOfTimes <= intParameter) {
            numberOfTimes++;

            RequestApi requestApi = new RequestApi();
            final String receivedResponse = requestApi.getRequest(200, APIPathes.twoRolls);
            CounterTwoDices.numberCounter(receivedResponse);

        }
        System.out.println(" 2= " + CounterTwoDices.countTwo + " 3= " + CounterTwoDices.countTree + " 4= " + CounterTwoDices.countFore + " 5= " + CounterTwoDices.countFive + " 6= " + CounterTwoDices.countSix + " 7= " + CounterTwoDices.countSeven + " 8= " + CounterTwoDices.countEight + " 9= " + CounterTwoDices.countNine + " 10= " + CounterTwoDices.countTen + " 11= " + CounterTwoDices.countEleven + " 12= " + CounterTwoDices.countTwelve);

    }
}
