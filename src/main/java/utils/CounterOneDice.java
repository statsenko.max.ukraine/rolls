package utils;

public class CounterOneDice {

    public static int countOne = 0;
    public static int countTwo = 0;
    public static int countTree = 0;
    public static int countFore = 0;
    public static int countFive = 0;
    public static int countSix = 0;

    public static void numberCounter(String str) {

        char[] arr = str.toCharArray();
        for (int i = 0; i < arr.length - 1; i++) {
            if (arr[i] == '1') {
                countOne++;
            } else if (arr[i] == '2') {
                countTwo++;
            } else if (arr[i] == '3') {
                countTree++;
            } else if (arr[i] == '4') {
                countFore++;
            } else if (arr[i] == '5') {
                countFive++;
            } else if (arr[i] == '6') {
                countSix++;
            }
        }
    }

    public static void percentDeviation(int parameter) {
        int oneSideOfCube = parameter * 100 / 6;
        double minimumValue = oneSideOfCube * 0.95;
        double maximumValue = oneSideOfCube * 1.05;
        if (minimumValue <= countOne && maximumValue >= countOne
                && minimumValue <= countTwo && maximumValue >= countTwo
                && minimumValue <= countTree && maximumValue >= countTree
                && minimumValue <= countFore && maximumValue >= countFore
                && minimumValue <= countFive && maximumValue >= countFive
                && minimumValue <= countSix && maximumValue >= countSix) {
            System.out.println("Maximum deviation less than 5%");
        } else System.out.println("Maximum deviation more than 5%");
    }
}
