package utils;

public class CounterTwoDices {

    public static int countTwo = 0;
    public static int countTree = 0;
    public static int countFore = 0;
    public static int countFive = 0;
    public static int countSix = 0;
    public static int countSeven = 0;
    public static int countEight = 0;
    public static int countNine = 0;
    public static int countTen = 0;
    public static int countEleven = 0;
    public static int countTwelve = 0;

    public static void numberCounter(String str) {
        String[] newStr = splitter(str);
        for (String s : newStr) {
            if (sumRolls(extractDigits(s)) == 2) {
                countTwo++;
            } else if (sumRolls(extractDigits(s)) == 3) {
                countTree++;
            } else if (sumRolls(extractDigits(s)) == 4) {
                countFore++;
            } else if (sumRolls(extractDigits(s)) == 5) {
                countFive++;
            } else if (sumRolls(extractDigits(s)) == 6) {
                countSix++;
            } else if (sumRolls(extractDigits(s)) == 7) {
                countSeven++;
            } else if (sumRolls(extractDigits(s)) == 8) {
                countEight++;
            } else if (sumRolls(extractDigits(s)) == 9) {
                countNine++;
            } else if (sumRolls(extractDigits(s)) == 10) {
                countTen++;
            } else if (sumRolls(extractDigits(s)) == 11) {
                countEleven++;
            } else if (sumRolls(extractDigits(s)) == 12) {
                countTwelve++;
            }
        }
    }

    public static String extractDigits(String src) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < src.length(); i++) {
            char c = src.charAt(i);
            if (Character.isDigit(c)) {
                builder.append(c);
            }
        }
        return builder.toString();
    }

    public static int sumRolls(String str) {
        char[] chars = str.toCharArray();
        return Character.getNumericValue(chars[0]) + Character.getNumericValue(chars[1]);
    }

    public static String[] splitter(String str) {
        return str.split("\n");
    }
}
