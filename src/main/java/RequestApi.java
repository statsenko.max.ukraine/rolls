import io.restassured.RestAssured;
import utils.APIPathes;


public class RequestApi extends AbstractApi {

    public String getRequest(int statusCode, String url) {
        return RestAssured.given()
                .contentType(io.restassured.http.ContentType.JSON)
                .get(url)
                .then()
                .statusCode(statusCode)
                .extract()
                .response()
                .asString();
    }
}
